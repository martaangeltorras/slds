var level5 = [
    {'type': 'zeppelin', 'y': 20, 'd':-1, 't': 5},
    {'type': 'helicopter', 'y': 40, 'd':-1, 't': 10},
    {'type': 'bomber', 'y': 15, 'd':1, 't': 15},
    {'type': 'bomber', 'y': 20, 'd':-1, 't': 20},
    {'type': 'plane', 'y': 50, 'd':1, 't': 24},
    {'type': 'zeppelin', 'y': 20, 'd':-1, 't': 30},
    {'type': 'bomber', 'y': 30, 'd':1, 't': 35},
    {'type': 'ambulancia', 'y': 20, 'd':-1, 't': 45},
    {'type': 'bomber', 'y': 50, 'd':-1, 't': 50},
    {'type': 'plane', 'y': 50, 'd':1, 't': 56},
    {'type': 'zeppelin', 'y': 20, 'd':-1, 't': 60},
    {'type': 'bomber', 'y': 20, 'd':-1, 't': 67},
    {'type': 'ambulancia', 'y': 20, 'd':-1, 't': 70}

];