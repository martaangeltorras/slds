var level3 = [
    {'type': 'bomber', 'y': 25, 'd':-1, 't': 5},
    {'type': 'plane', 'y': 10, 'd':1, 't': 12},
    {'type': 'helicopter', 'y': 60, 'd':-1, 't': 20},
    {'type': 'bomber', 'y': 15, 'd':-1, 't': 25},
    {'type': 'plane', 'y': 15, 'd':1, 't': 30},
    {'type': 'bomber', 'y': 15, 'd':1, 't': 40},
    {'type': 'bomber', 'y': 20, 'd':-1, 't': 49},
    {'type': 'plane', 'y': 50, 'd':1, 't': 53},
    {'type': 'bomber', 'y': 30, 'd':1, 't': 55},
    {'type': 'bomber', 'y': 50, 'd':-1, 't': 65},
    {'type': 'plane', 'y': 50, 'd':1, 't': 75}

];

