var play = new Kiwi.State('play');



var Platform = function (state, x, y){
    Kiwi.GameObjects.Sprite.call(this, state, state.textures['ground'], x, y, true);
    this.physics = this.components.add(new Kiwi.Components.ArcadePhysics(this, this.box));
    this.physics.immovable = true;

    Platform.prototype.update = function(){
        Kiwi.GameObjects.Sprite.prototype.update.call(this);
        this.physics.update();
    }

}
Kiwi.extend(Platform,Kiwi.GameObjects.Sprite);

play.preload = function(){
    Kiwi.State.prototype.preload.call(this);
    ///////////////////////////////////////
    //Environment Assets
    this.addImage('ground', 'assets/ground.png');
    this.addImage('grass', 'assets/ground-tiles/grass.png');
    this.addImage('dirt', 'assets/ground-tiles/dirt.png');
    this.addImage('bg1', 'assets/bg-layers/1.png');
    this.addImage('bg2', 'assets/bg-layers/2.png');
    this.addImage('bg3', 'assets/bg-layers/3.png');
    this.addImage('bg4', 'assets/bg-layers/4.png');
    this.addImage('bg5', 'assets/bg-layers/5.png');
    this.addImage('bg6', 'assets/bg-layers/6.png');
    this.addImage('bg7', 'assets/bg-layers/7.png');

    this.addSpriteSheet('tank_body', 'assets/tank/body.png', 148, 68);
    this.addImage('tank_head',   'assets/tank/head.png');
    this.addImage('tank_cannon', 'assets/tank/cannon.png');

    this.addImage('jet', 'assets/enemies/jet.png');
    this.addImage('bomber', 'assets/enemies/bomber.png');

    this.addImage('zeppelin', 'assets/enemies/zeppelin.png');

    this.addSpriteSheet('particles', 'assets/particles.png', 170, 170);
    this.addImage('bomb', 'assets/bomb.png');
    this.addImage('bullet', 'assets/bullet.png');
    this.addSpriteSheet('explosion', 'assets/explosion.png', 100, 100);
    this.addSpriteSheet('helicopter', 'assets/enemies/helicopter.png',200,94.6);

    this.addSpriteSheet('ambulancia', 'assets/enemies/ambulancia.png',250,81.6);  
    this.addSpriteSheet('zeppelin', 'assets/enemies/zeppelin.png',110,83);
    this.addSpriteSheet('bombazepelin', 'assets/bombaZepelin.png',35,70); 
    this.addImage('box', 'assets/enemies/caixa.png');


    this.addImage('backgroundPortada', 'assets/menu/backgroundPortada.png');
}

play.create = function(){
    this.ground = new Platform(this, 0, 505);
    //////////////////////////////
    //Parallax Environment Groups
    this.grassGroup = new Kiwi.Group(this);
    this.bg1 = new Kiwi.Group(this);
    this.bg2 = new Kiwi.Group(this);
    this.bg3 = new Kiwi.Group(this);
    this.bg4 = new Kiwi.Group(this);
    this.bg5 = new Kiwi.Group(this);
    this.bg6 = new Kiwi.Group(this);
    this.bg7 = new Kiwi.Group(this);

    this.enemies = [];
    this.bombs = [];
    this.bullets = [];

    this.gameOver = false;
    this.showOver = false;

    this.stateKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.ENTER);

    this.timestamp = 0;
    this.i = 0;
    this.j = 150;

    ////////////////////////
    //Creating parallax bacground assets
    for(var i = 0; i < 20; i++){//grass
        this.grassGroup.addChild(new Kiwi.GameObjects.Sprite(this, this.textures['grass'], i * 48, 504, true));
        this.grassGroup.addChild(new Kiwi.GameObjects.Sprite(this, this.textures['dirt'], i * 48, 552, true));

    }
    for(var i = 0; i < 4; i++){
        this.bg7.addChild(new Kiwi.GameObjects.Sprite(this, this.textures['bg7'], i*434, 0, true));//bg7
    }
    for(var i = 0; i < 5; i++){
        this.bg6.addChild(new Kiwi.GameObjects.Sprite(this, this.textures['bg6'], i*346, 185, true));//bg6
    }
    for(var i = 0; i < 10; i++){
        this.bg5.addChild(new Kiwi.GameObjects.Sprite(this, this.textures['bg5'], i*96, 253, true));//bg5
        this.bg4.addChild(new Kiwi.GameObjects.Sprite(this, this.textures['bg4'], i*96, 279, true));//bg4
    }
    for(var i = 0; i < 3; i++){
        this.bg3.addChild(new Kiwi.GameObjects.Sprite(this, this.textures['bg3'], i*460, 305, true));//bg3
        this.bg2.addChild(new Kiwi.GameObjects.Sprite(this, this.textures['bg2'], i*460, 335, true));//bg2
        this.bg1.addChild(new Kiwi.GameObjects.Sprite(this, this.textures['bg1'], i*460, 381, true));//bg1
    }
    //Background
    this.addChild(this.ground);
    this.addChild(this.bg7);
    this.addChild(this.bg6);
    this.addChild(this.bg5);
    this.addChild(this.bg4);
    this.addChild(this.bg3);
    this.addChild(this.bg2);
    this.addChild(this.bg1);

    //Foreground
    this.addChild(this.grassGroup);

    this.tank = new Tank(this, 200, 436);


}

play.update = function(){

    if (!this.gameOver && level != null && this.i < level.length) {
        if (level[this.i].t <= this.timestamp / 20) {
            var enemy = level[this.i];
            switch (enemy.type) {
                case 'plane':
                    this.enemies.push(new Plane(this, enemy.y, enemy.d));
                    break;
                case 'bomber':
                    this.enemies.push(new Bomber(this, enemy.y, enemy.d));
                    break;
                case 'zeppelin':
                    this.enemies.push(new Zeppelin(this, enemy.y, enemy.d));
                    break;
                case 'helicopter':
                    this.enemies.push(new Helicopter(this, enemy.y, enemy.d,0));
                    break;
                case 'ambulancia':
                    this.enemies.push(new Ambulancia(this, enemy.y, enemy.d));
                    break;
                case 'zeppelin':
                    this.enemies.push(new Zeppelin(this, enemy.y, enemy.d));
                    break;
                case 'box':
                    this.enemies.push(new Box(this, 200, enemy.y));
                    break;
            }
            this.i++;
        }
    }
    if (level == null) {
        this.i--;
        if (this.i <= 0) {
            var e = Math.random();
            var y = Math.random()*60+10;
            var d = Math.random()>0.5?-1:1;
            if (e < 0.3) {
                this.enemies.push(new Plane(this, y, d));
            } else if (e < 0.6) {
                this.enemies.push(new Bomber(this, y, d));
            } else if (e < 0.85) {
                this.enemies.push(new Zeppelin(this, y, d));
            } else if (e < 0.95) {
                this.enemies.push(new Helicopter(this, y, d,0));
            } else {
                this.enemies.push(new Ambulancia(this, y, d));
            }

            this.i = Math.random()*this.j+this.j;
            console.log("i: "+this.i);
            this.j--;
            console.log("j: "+this.j);
        }
    }
    if (this.gameOver) {
        if (this.game.input.mouse.isDown) {
            this.game.states.switchState("InicialState");
            this.create();
        }
    }


    if (level != null && this.i >= level.length && this.enemies.length == 0) {

        if (!this.showOver) {
            this.showOver = true;
            this.addChild(new Kiwi.GameObjects.Textfield(this, 'CONGRATULATIONS!', 250, 200, '#000000', 32, 'bold', 'serif'));
            this.addChild(new Kiwi.GameObjects.Textfield(this, 'YOU PASS TO THE NEXT LEVEL', 150, 240, '#000000', 32, 'bold', 'serif'));
            this.addChild(new Kiwi.GameObjects.Textfield(this, 'Pres ENTER to CONTINUE', 200, 400, '#ff0000', 40, 'bold', 'serif'));
        }

        if(this.stateKey.isDown){
            arcadeState.increaseLevel();
            this.game.states.switchState("ArcadeState");
            this.create();
        }
    }


    var x = 0;
    for (var j in this.enemies) {
        var enemy = this.enemies[j];
        if (!enemy instanceof Ambulancia)  x = 1;
    }

    if (level != null && this.i >= level.length && x == 0) {
        this.showOver = true;
        this.addChild(new Kiwi.GameObjects.Textfield(this, 'CONGRATULATIONS!', 250, 200, '#000000', 32, 'bold', 'serif'));
        this.addChild(new Kiwi.GameObjects.Textfield(this, 'YOU PASS TO THE NEXT LEVEL', 150, 240, '#000000', 32, 'bold', 'serif'));
        this.addChild(new Kiwi.GameObjects.Textfield(this, 'Pres ENTER to CONTINUE', 200, 400, '#ff0000', 40, 'bold', 'serif'));
        
        if(this.stateKey.isDown){
            arcadeState.increaseLevel();
            this.game.states.switchState("ArcadeState");
            this.create();
        }
    }

    this.timestamp++;

    Kiwi.State.prototype.update.call(this);

    this.updateParallax();

    var bulletsRemove = [];
    var enemiesRemove = [];
    for (var i in this.bullets) {
        var bullet = this.bullets[i];
        if (bullet._parent == undefined) {
            bulletsRemove.push(i);
        } else {
            for (var j in this.enemies) {
                var enemy = this.enemies[j];
                if (!enemy._visible) {
                    enemiesRemove.push(j);
                } else {
                    if(bullet.box.bounds.intersects(enemy.box.bounds)) {
                        if (enemy instanceof Ambulancia) {
                            this.tank.lives--;
                        }
                        this.explosion(bullet, enemy);
          
                        this.enemies.splice(j,1);
                    }
                }
            }
        }
    }




    for (var i in bulletsRemove) {
        console.log("removed bullet " + i);
        this.bullets.splice(i, 1);
    }
    /*
    for (var i in enemiesRemove) {

    }*/

    var bombsRemove = [];
    for (var i in this.bombs) {
        var bomb = this.bombs[i];
        if (bomb._parent == undefined) {
            bombsRemove.push(i);
        } else if (this.tank._visible) {
            if(bomb.box.bounds.intersects(this.tank.box.bounds)) {
                this.explosion(bomb, null);
                this.tank.lives--;
            }
        }
    }

    if (!this.tank._visible) {
        if (!this.gameOver) {
            this.text2 = new Kiwi.GameObjects.Textfield(this, 'GAME OVER', 250, 200, '#000000',  48, 'bold', 'serif');
            this.addChild(this.text2);
            this.text2 = new Kiwi.GameObjects.Textfield(this, 'CLICK to CONTINUE', 200, 400, '#ff0000',  40, 'bold', 'serif');
            this.addChild(this.text2);
        }
        this.gameOver = true;

    }

}

play.updateParallax = function(){
    //Ground
    for(var i =0; i < this.grassGroup.members.length;i++){
        this.grassGroup.members[i].transform.x -= 1;
        if(this.grassGroup.members[i].transform.worldX <= -48){
            this.grassGroup.members[i].transform.x = 48*19;
        }
    }
    //bg1
    for(var i =0; i < this.bg1.members.length;i++){
        this.bg1.members[i].transform.x -= 1;
        if(this.bg1.members[i].transform.worldX <= -460){
            this.bg1.members[i].transform.x = 460* (this.bg1.members.length - 1) ;
        }
    }
    //bg2
    for(var i =0; i < this.bg2.members.length;i++){
        this.bg2.members[i].transform.x -= 0.5;
        if(this.bg2.members[i].transform.worldX <= -460){
            this.bg2.members[i].transform.x = 460*(this.bg2.members.length - 1);
        }
    }
    //bg3
    for(var i =0; i < this.bg3.members.length;i++){
        this.bg3.members[i].transform.x -= 0.3;
        if(this.bg3.members[i].transform.worldX <= -460){
            this.bg3.members[i].transform.x = 460*(this.bg3.members.length - 1);
        }
    }
    //bg4
    for(var i =0; i < this.bg4.members.length;i++){
        this.bg4.members[i].transform.x -= 0.2;
        if(this.bg4.members[i].transform.worldX <= -96){
            this.bg4.members[i].transform.x = 96*(this.bg4.members.length - 1);
        }
    }
    //bg5
    for(var i =0; i < this.bg4.members.length;i++){
        this.bg5.members[i].transform.x -= 0.1;
        if(this.bg5.members[i].transform.worldX <= -96){
            this.bg5.members[i].transform.x = 96*(this.bg5.members.length - 1);
        }
    }

    //bg7
    for(var i =0; i < this.bg7.members.length;i++){
        this.bg7.members[i].transform.x -= .25;
        if(this.bg7.members[i].transform.worldX <= -434){
            this.bg7.members[i].transform.x = 434*(this.bg7.members.length - 1);
        }
    }

}

play.addBombaZepelin = function(x,y) {
    var bomb = new BombaZ(this, x, y);
    this.bombs.push(bomb);
    this.addChild(bomb);
}



play.addBomb = function(x,y) {
    var bomb = new Bomb(this, x, y);
    this.bombs.push(bomb);
    this.addChild(bomb);
}

play.addBullet = function(x, y, angle) {
    var bullet = new Bullet(this, x, y, angle);
    this.bullets.push(bullet);
    this.addChild(bullet);
}

play.explosion = function(elem1, elem2) {
    this.addChild(new Explosion(this, elem1.x, elem1.y));
    Kiwi.Entity.prototype.destroy.call(elem1);
    Kiwi.Entity.prototype.destroy.call(elem2);
}


