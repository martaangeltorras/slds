var Box = function(state,x,y ) {

    this.velocity = 1;

    Kiwi.GameObjects.Sprite.call(this, state, state.textures['box'], x, y, true);

    Box.prototype.update = function() {
        Kiwi.GameObjects.Sprite.prototype.update.call(this);
        this.x -= this.velocity;
        state.addChild(this);

        if (this.x < -200) Kiwi.Entity.prototype.destroy.call(this);
        
    }
}
Kiwi.extend(Box,Kiwi.GameObjects.Sprite);
