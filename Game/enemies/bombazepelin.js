var BombaZ = function(state, x, y ) {
    Kiwi.GameObjects.Sprite.call(this, state, state.textures['bombazepelin'], x, y, true);
    this.physics = this.components.add(new Kiwi.Components.ArcadePhysics(this, this.box));

    this.physics.acceleration = new Kiwi.Geom.Point( 0, 5 );
    this.physics.velocity = new Kiwi.Geom.Point( 0, 0 );

    this.state = state;

    BombaZ.prototype.update = function () {
        Kiwi.GameObjects.Sprite.prototype.update.call( this );
        //Update ArcadePhysics
        this.physics.update();

        if (this.y > 470) {
            this.state.explosion(this, null);
        }
    }
}
Kiwi.extend(BombaZ,Kiwi.GameObjects.Sprite);

