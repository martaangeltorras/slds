var Tank = function (state, x, y) {
    Kiwi.GameObjects.Sprite.call(this, state, state.textures['tank_body'], x, y, true);

    this.maxSpeed = 3;
    //this.animation.add('run', [1, 2, 3, 4, 5, 6], 0.1, true);
    //this.animation.add('die', [11, 12, 13, 14], 0.1, false);
    //this.animation.play('run');

    this.head = new Kiwi.GameObjects.Sprite(state, state.textures['tank_head'], x, y, true);
    this.head.anchorPointX = 30;

    this.cannon = new Kiwi.GameObjects.Sprite(state, state.textures['tank_cannon'], x,y, true);
    this.cannon.anchorPointX = 0;
    this.cannon.anchorPointY = 10;

    state.addChild(this.cannon);
    state.addChild(this.head);
    state.addChild(this);

    this.tankDirt = new Kiwi.GameObjects.StatelessParticles(state, state.textures['particles'], 0, 0, tankDirtConfig );
    this.tankDirt.startEmitting( true, false );
    state.addChild(this.tankDirt);

    this.lastBullet = 0;
    this.speedBullet = 30;

    this.lives = 3;
    this.textLives = new Kiwi.GameObjects.Textfield(state, 'Lives: 3', 700, 550, '#ff0000',  24, 'normal', 'serif');
    state.addChild(this.textLives);

    this.state = state;
    Tank.prototype.update = function(){
        Kiwi.GameObjects.Sprite.prototype.update.call(this);

        if (this.lastBullet > 0) this.lastBullet--;

        var dx = this.game.input.mouse.x - this.x - 80;

        if (Math.abs(dx) > 50) {
            this.x = Math.max(5, Math.min(660, this.x + Math.max(-this.maxSpeed-1,
                    Math.min(this.maxSpeed,
                        dx))));
            this.tankDirt.x = this.x;
        }


        var x = this.game.input.mouse.x - this.x - 70;
        var y = -this.game.input.mouse.y + this.y -15;
        var a = Math.atan2(x,y);

        this.head.x = this.x+43;
        this.head.y = this.y-48;
        if (x < 0) this.head.scaleX = -1;
        else this.head.scaleX = 1;

        this.cannon.x = this.x+70;
        this.cannon.y = this.y-28;
        this.cannon.rotation = a - Math.PI/2;
        if (this.cannon.rotation > 0.1) this.cannon.rotation = 0.1;
        if (this.cannon.rotation < -3.3) this.cannon.rotation = -3.3;

        if (this.game.input.mouse.isDown) {
            if (this.lastBullet == 0) {
                this.state.addBullet(this.cannon.x, this.cannon.y, this.cannon.rotation);
                this.lastBullet = this.speedBullet;
            }
        }
        if (this.lives <= 0) {
            this.lives = 0;
            Kiwi.Entity.prototype.destroy.call(this);
        }
        this.textLives.text = 'Lives: '+this.lives;
    }

    Tank.prototype.destroy = function() {
        Kiwi.Entity.prototype.destroy.call(this.head);
        Kiwi.Entity.prototype.destroy.call(this.cannon);
        Kiwi.Entity.prototype.destroy.call(this.tankDirt);
    }
}
Kiwi.extend(Tank,Kiwi.GameObjects.Sprite);
