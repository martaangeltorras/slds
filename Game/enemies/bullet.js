var Bullet = function(state, x, y, angle ) {
    Kiwi.GameObjects.Sprite.call(this, state, state.textures['bullet'], x+80*Math.cos(angle)+2*Math.sin(angle), y+80*Math.sin(angle)+2*Math.cos(angle), true);
    this.rotation = angle;
    Bullet.prototype.update = function () {
        Kiwi.GameObjects.Sprite.prototype.update.call( this );
        this.x += 10*Math.cos(this.rotation);
        this.y += 10*Math.sin(this.rotation);

        if (this.y < -20 || this.x < -20 || this.x > 820 || this.y > 620) {
            Kiwi.Entity.prototype.destroy.call(this);
        }
    }
}
Kiwi.extend(Bullet,Kiwi.GameObjects.Sprite);