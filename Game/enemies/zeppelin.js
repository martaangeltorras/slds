
var Zeppelin = function (state, y, direction) {
    var x = -300;
    if (direction < 0) {
        x = 810; // TODO
    }
    this.velocity = direction*1;
    Kiwi.GameObjects.Sprite.call(this, state, state.textures['zeppelin'], x, y, true);

    if (direction < 0) { // s'ha de fer dp de creat el sprite
        this.scaleX = -1;
        smokeConfig.gravityY = -33;
        smokeConfig.posOffsetX = 211;
    } else {
        this.scaleX = 1;
        smokeConfig.gravityX = 33;
        smokeConfig.posOffsetX = 80;
    }

    this.smoke = new Kiwi.GameObjects.StatelessParticles(state, state.textures['particles'], 0, 0, smokeConfig );
    this.smoke.startEmitting( true, false );

    this.state = state;

    state.addChild(this.smoke);
    state.addChild(this);


    Zeppelin.prototype.update = function(){
        Kiwi.GameObjects.Sprite.prototype.update.call(this);
        this.x += this.velocity;
        this.smoke.x = this.x;
        this.smoke.y = this.y;
        if (this.x % 100 == 0) {
            this.state.addBombaZepelin(this.x+80, this.y+60);
        }

        if (this.x < -400 || this.x > 1200) {
            Kiwi.Entity.prototype.destroy.call(this);
        }
    }

    Zeppelin.prototype.destroy = function() {
        Kiwi.Entity.prototype.destroy.call(this.smoke);
    }
}
Kiwi.extend(Zeppelin,Kiwi.GameObjects.Sprite);
