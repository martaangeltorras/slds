var Explosion = function(state,x,y ) {

    this.velocity = 1;

    Kiwi.GameObjects.Sprite.call(this, state, state.textures['explosion'], x-50, y-50, true);

    Explosion.prototype.update = function() {
        Kiwi.GameObjects.Sprite.prototype.update.call(this);

    }
    this.stop = function() {
        Kiwi.Entity.prototype.destroy.call(this);
    }

    var anim = this.animation.add( 'run', [0,1,2,3,4,5,6,7], 0.1, false );
    anim.onStop.add(this.stop, this);
    this.animation.play('run');
}
Kiwi.extend(Explosion,Kiwi.GameObjects.Sprite);
