var Ambulancia = function (state, y, direction) {
    var x = 0
    if (direction < 0) {
        x = 800; // TODO
    }
    this.velocity = direction*0.9;
    

    Kiwi.GameObjects.Sprite.call(this, state, state.textures['ambulancia'], x, y, true);

    this.animation.add('run', [0, 1, 2], 0.1, true);
    this.animation.play('run');

    if (direction < 0) { // s'ha de fer dp de creat el sprite
        this.scaleX = -1;
    }
    this.sate = state;

   /* this.animation.add('run', [0, 1, 2], 0.1, true);
    this.animation.play('run');*/
    state.addChild(this);

    Ambulancia.prototype.update = function(){

        Kiwi.GameObjects.Sprite.prototype.update.call(this);
        this.x += this.velocity;

        if (this.x < -200 || this.x > 1000) {
            Kiwi.Entity.prototype.destroy.call(this);
        }
    }

}
Kiwi.extend(Ambulancia,Kiwi.GameObjects.Sprite);
