var Car = function (state, y, direction) {
    var x = 0
    if (direction < 0) {
        x = 800; // TODO
    }
    this.velocity = direction*2;
    Kiwi.GameObjects.Sprite.call(this, state, state.textures['car'], x, y, true);

    if (direction > 0) { // s'ha de fer dp de creat el sprite
        this.scaleX = -1;
    }
    this.sate = state;

    this.animation.add('run', [0], 0.1, true);
    this.animation.play('run');

    Car.prototype.update = function(){
        Kiwi.GameObjects.Sprite.prototype.update.call(this);
        this.x += this.velocity;

        if (this.x < -200 || this.x > 1000) {
            Kiwi.Entity.prototype.destroy.call(this);
        }
    }

}
Kiwi.extend(Car,Kiwi.GameObjects.Sprite);
