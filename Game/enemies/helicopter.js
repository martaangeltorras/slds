var Helicopter = function (state,y,direction,x) {
    this.dir = 'br';

    this.velocity = direction*2.0;
    Kiwi.GameObjects.Sprite.call(this, state, state.textures['helicopter'], x, y, true);

    this.animation.add('run', [0, 1, 2], 0.1, true);
    this.animation.play('run');

    state.addChild(this);

    Helicopter.prototype.update = function(){

        Kiwi.GameObjects.Sprite.prototype.update.call(this);

        if(this.dir == 'br'){
            this.x += 3.5;
            this.y += 0.5;

            if(this.x > 580 && this.y > 80){
                this.dir = 't';
            }
        }
        else if(this.dir == 'bl'){
            this.x -= 3.5;
            this.y += 0.5;
            if(this.x < 20 && this.y > 80){
                this.dir = 't';
            }
        }
        else{ // dir == 't'
            this.y -= 1.5

        if(this.y < 20){
                if(this.scaleX == 1) dir = 'bl';
                else this.dir = 'br';
                this.scaleX *= -1;
            }
        }

/*

        if(gira == false && puja == false) {
            this.x +=3.3;
            this.y += 0.51;
            this.scaleX = 1;
        }
 //primera volta
        if (this.x >= 450 && this.y >= 20) {
                puja = true;
                gira = false;
        }

        if(this.x > 450 && this.y <= 30) {
                gira = true;
                puja = false;
        }

        if(this.x <= 20 && this.y >= 10) {
            puja = true;
            gira = false;

        }

        if (this.x < 20 && this.y < 20) {
            puja = false;
            gira = false;
        }

        if (!gira && puja)  {
            this.y -=3;
            this.scaleX = 1;
        }

        else if (gira && !puja) {
            this.y +=0.51;
            this.x -=3.03;
            this.scaleX = -1;
        }
        if (this.y < 20) puja = false;
        if (this.x < 20) gira = false;

*/

        if (this.x < -10) {
            Kiwi.Entity.prototype.destroy.call(this);
        } 
    }

}
Kiwi.extend(Helicopter,Kiwi.GameObjects.Sprite);
