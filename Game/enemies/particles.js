var tankDirtConfig =
{
    "additive": false,
    "alpha": 1,
    "alphaGradient": [
        1,
        1,
        1,
        0
    ],
    "alphaStops": [
        0.3,
        0.7
    ],
    "angStartMin": 3.14,
    "angStartMax": 0,
    "angVelocityConform": true,
    "numParts": 80,
    "colEnv0": [
        0.6470588235294118,
        0.32941176470588235,
        0
    ],
    "colEnv1": [
        0.5176470588235295,
        0.21176470588235294,
        0
    ],
    "colEnv2": [
        0.3411764705882353,
        0.16470588235294117,
        0
    ],
    "colEnv3": [
        0,
        0,
        0
    ],
    "colEnvKeyframes": [
        0.5,
        0.6
    ],
    "endSize": 42,
    "gravityY": -20,
    "loop": true,
    "maxLifespan": 2,
    "maxStartTime": 6,
    "maxVel": 100,
    "minLifespan": 1,
    "minStartTime": 0,
    "minVel": 70,
    "posAngle": 0,
    "posConstrainRadial": true,
    "posConstrainRect": true,
    "posLength": 80,
    "posOffsetX": 80,
    "posOffsetY": 504,
    "posRadialStart": 4,
    "posRadialEnd": 5,
    "posRadius": 1,
    "posShape": "line",
    "posWidth": 100,
    "startSize": 0,
    "velAngle": 0,
    "velAngMin": -4,
    "velAngMax": 1.1,
    "velConstrainRadial": false,
    "velConstrainRect": false,
    "velHeight": 100,
    "velLength": 1,
    "velOffsetX": -46,
    "velOffsetY": 0,
    "velRadialStart": 0,
    "velRadialEnd": 6.283185307179586,
    "velRadius": 100,
    "velShape": "line",
    "velWidth": 100,
    "cells": [8]
};

var jetFuelConfig = {
        "additive": false,
        "alpha": 1,
        "alphaGradient": [
            1,
            1,
            1,
            0
        ],
        "alphaStops": [
            0.3,
            0.7
        ],
        "angStartMin": 0,
        "angStartMax": 0,
        "angVelocityConform": false,
        "numParts": 100,
        "colEnv0": [
            1,
            0,
            0
        ],
        "colEnv1": [
            1,
            1,
            0
        ],
        "colEnv2": [
            1,
            1,
            1
        ],
        "colEnv3": [
            0,
            0,
            0
        ],
        "colEnvKeyframes": [
            0.5,
            0.6
        ],
        "endSize": 52,
        "gravityX": 40,
        "gravityY": 0,
        "loop": true,
        "maxLifespan": 3,
        "maxStartTime": 6,
        "maxVel": 100,
        "minLifespan": 3,
        "minStartTime": 0,
        "minVel": 70,
        "posAngle": 0,
        "posConstrainRadial": true,
        "posConstrainRect": true,
        "posHeight": 100,
        "posLength": 100,
        "posOffsetX": 166,
        "posOffsetY": 55,
        "posRadialStart": 4.363323129985823,
        "posRadialEnd": 5.061454830783556,
        "posRadius": 1,
        "posShape": "point",
        "posWidth": 100,
        "startSize": 18,
        "velAngle": 0,
        "velAngMin": -2,
        "velAngMax": 2,
        "velConstrainRadial": false,
        "velConstrainRect": false,
        "velHeight": 100,
        "velLength": 105,
        "velOffsetX": 0,
        "velOffsetY": 0,
        "velRadialStart": 0,
        "velRadialEnd": 6.283185307179586,
        "velRadius": 100,
        "velShape": "point",
        "velWidth": 100,
        "cells": [8]
};

var smokeConfig = {
    "additive": false,
    "alpha": 1,
    "alphaGradient": [
    1,
    1,
    1,
    0
],
    "alphaStops": [
    0.3,
    0.7
],
    "angStartMin": 0,
    "angStartMax": 0,
    "angVelocityConform": false,
    "numParts": 100,
    "colEnv0": [
    0.00784313725490196,
    0,
    0
],
    "colEnv1": [
    0.5529411764705883,
    0.5529411764705883,
    0.5529411764705883
],
    "colEnv2": [
    1,
    1,
    1
],
    "colEnv3": [
    0,
    0,
    0
],
    "colEnvKeyframes": [
    0.5,
    0.6
],
    "endSize": 74,
    "gravityX": 33,
    "gravityY": 0,
    "loop": true,
    "maxLifespan": 4,
    "maxStartTime": 6,
    "maxVel": 100,
    "minLifespan": 2,
    "minStartTime": 0,
    "minVel": 70,
    "posAngle": 0,
    "posConstrainRadial": true,
    "posConstrainRect": true,
    "posHeight": 100,
    "posLength": 100,
    "posOffsetX": 90,
    "posOffsetY": 10,
    "posRadialStart": 4.363323129985823,
    "posRadialEnd": 5.061454830783556,
    "posRadius": 1,
    "posShape": "point",
    "posWidth": 100,
    "startSize": 12,
    "velAngle": 0,
    "velAngMin": -2,
    "velAngMax": 2,
    "velConstrainRadial": false,
    "velConstrainRect": false,
    "velHeight": 100,
    "velLength": 105,
    "velOffsetX": 0,
    "velOffsetY": -42,
    "velRadialStart": 0,
    "velRadialEnd": 6.283185307179586,
    "velRadius": 100,
    "velShape": "point",
    "velWidth": 100,
    "cells": [8]
};