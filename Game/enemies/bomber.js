var Bomber = function (state, y, direction) {
    var x = -100;
    if (direction < 0) {
        x = 900; // TODO
    }
    this.velocity = direction*2.0;
    Kiwi.GameObjects.Sprite.call(this, state, state.textures['bomber'], x, y, true);

    if (direction < 0) { // s'ha de fer dp de creat el sprite
        this.scaleX = -1;
        jetFuelConfig.gravityX = 50;
        jetFuelConfig.posOffsetX = 165;
    } else {
        this.scaleX = 1;
        jetFuelConfig.posOffsetX = 0;
        jetFuelConfig.gravityX = -50;
    }
    this.fuel = new Kiwi.GameObjects.StatelessParticles(state, state.textures['particles'], 0, 0, jetFuelConfig );
    this.fuel.startEmitting( true, false );

    this.state = state;

    state.addChild(this.fuel);
    state.addChild(this);

    Bomber.prototype.update = function(){
        Kiwi.GameObjects.Sprite.prototype.update.call(this);
        this.x += this.velocity;
        this.fuel.x = this.x;
        this.fuel.y = this.y;

        if (this.x % 100 == 0) {
            this.state.addBomb(this.x+50, this.y+80);
        }

        if (this.x < -200 || this.x > 1000) {
            Kiwi.Entity.prototype.destroy.call(this);
        }
    }

    Bomber.prototype.destroy = function() {
        Kiwi.Entity.prototype.destroy.call(this.fuel);
    }


}
Kiwi.extend(Bomber,Kiwi.GameObjects.Sprite);