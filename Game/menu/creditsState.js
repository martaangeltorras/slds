﻿var creditsState = new Kiwi.State('CreditsState');

creditsState.create = function(){
	Kiwi.State.prototype.create.call(this);

	//BACKGROUND
    this.background = new Kiwi.GameObjects.StaticImage(this, this.textures['backgroundCredits'], 0, 0);
    this.addChild(this.background);

    //TITOL
    this.text1 = new Kiwi.GameObjects.Textfield(this, 'CRÈDITS', 60, 100, '#ffffff',  48, 'bold', 'serif');
    this.addChild(this.text1);

    //CREADORS
    this.text2 = new Kiwi.GameObjects.Textfield(this, 'Creadors', 60, 200, '#ffffff',  40, 'normal', 'serif');
    this.addChild(this.text2);
    this.text3 = new Kiwi.GameObjects.Textfield(this, 'Marta Àngel', 100, 250, '#ffffff',  24, 'normal', 'serif');
    this.addChild(this.text3);
    this.text4 = new Kiwi.GameObjects.Textfield(this, 'Elena Aragón', 100, 280, '#ffffff',  24, 'normal', 'serif');
    this.addChild(this.text4);
    this.text5 = new Kiwi.GameObjects.Textfield(this, 'Jessica Borges', 100, 310, '#ffffff',  24, 'normal', 'serif');
    this.addChild(this.text5);
    this.text6 = new Kiwi.GameObjects.Textfield(this, 'Pau Vila', 100, 340, '#ffffff',  24, 'normal', 'serif');
    this.addChild(this.text6);
    this.text7 = new Kiwi.GameObjects.Textfield(this, 'Joan Viladrosa', 100, 370, '#ffffff',  24, 'normal', 'serif');
    this.addChild(this.text7);

    //ASSIGNATURA I ANY
	this.text8 = new Kiwi.GameObjects.Textfield(this, 'SLDS - Projecte de Software - Maig 2015', 200, 550, '#ffffff',  24, 'normal', 'serif');
    this.addChild(this.text8);

    //BUTONS
    //BACK
    this.backButton = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['rightButton'], 470,290);
	this.addChild(this.backButton);
	this.text8 = new Kiwi.GameObjects.Textfield(this, 'Back', 530, 310, '#ffffff',  24, 'bold', 'serif');
    this.addChild(this.text8);
}

creditsState.update = function(){
	Kiwi.State.prototype.update.call(this);

	if(this.backButton.isDown){
		this.game.states.switchState("InicialState");
	}
}