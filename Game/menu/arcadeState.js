var arcadeState = new Kiwi.State('ArcadeState');

arcadeState.create = function() {
    Kiwi.State.prototype.create.call(this);

    this.currentLevel = 0;
    //BACKGROUND
    this.background = new Kiwi.GameObjects.StaticImage(this, this.textures['backgroundArcade'], 0, 0);
    this.addChild(this.background);

    //TITOL
    this.text1 = new Kiwi.GameObjects.Textfield(this, 'Arcade Mode', 30, 20, '#ffffff', 48, 'bold', 'serif');
    this.addChild(this.text1);

    //MAPA
    this.mapa = new Kiwi.GameObjects.StaticImage(this, this.textures['mapa'], 30, 80);
    this.addChild(this.mapa);

    //BUTONS
    //BACK
    this.backButton = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['rightButton'], 660, 10);
    this.addChild(this.backButton);
    this.text2 = new Kiwi.GameObjects.Textfield(this, 'Back', 720, 30, '#ffffff', 24, 'bold', 'serif');
    this.addChild(this.text2);

    //BANDERA1
    this.level1Button = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['island'], 125, 125);
    this.addChild(this.level1Button);
    this.text1 = new Kiwi.GameObjects.Textfield(this, '1', 130, 130, '#ff0000', 24, 'bold', 'serif');
    this.addChild(this.text1);

    //BANDERA2
    this.level2Button = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['beach'], 205, 345);
    this.addChild(this.level2Button);
    this.text2 = new Kiwi.GameObjects.Textfield(this, '2', 210, 350, '#ff0000', 24, 'bold', 'serif');
    this.addChild(this.text2);

    //BANDERA3
    this.level3Button = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['wood'], 295, 455);
    this.addChild(this.level3Button);
    this.text3 = new Kiwi.GameObjects.Textfield(this, '3', 300, 460, '#ff0000', 24, 'bold', 'serif');
    this.addChild(this.text3);

    //BANDERA4
    this.level4Button = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['snow'], 435, 375);
    this.addChild(this.level4Button);
    this.text4 = new Kiwi.GameObjects.Textfield(this, '4', 440, 380, '#ff0000', 24, 'bold', 'serif');
    this.addChild(this.text4);

    //BANDERA5
    this.level5Button = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['desert'], 485, 175);
    this.addChild(this.level5Button);
    this.text5 = new Kiwi.GameObjects.Textfield(this, '5', 490, 180, '#ff0000', 24, 'bold', 'serif');
    this.addChild(this.text5);

    //BANDERA6
    this.level6Button = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['beach'], 675, 285);
    this.addChild(this.level6Button);
    this.text6 = new Kiwi.GameObjects.Textfield(this, '6', 680, 290, '#ff0000', 24, 'bold', 'serif');
    this.addChild(this.text6);

    //BANDERA7
    this.level6Button = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['water'], 675, 285);
    this.addChild(this.level6Button);
    this.text6 = new Kiwi.GameObjects.Textfield(this, '7', 680, 290, '#ff0000', 24, 'bold', 'serif');
    this.addChild(this.text6);

    console.log(this.game);
    if (this.game.saveManager.localStorage.exists('maxLevel')) {
        this.maxLevel = this.game.saveManager.localStorage.getData('maxLevel');
    } else {
        this.maxLevel = 1;
    }
}


arcadeState.update = function(){
	Kiwi.State.prototype.update.call(this);

	if(this.backButton.isDown){
		this.game.states.switchState("InicialState");
	}

    this.level1Button.visible = this.text1.visible = (this.maxLevel >=1);
	if(this.level1Button.isDown){
    
        level = level1;
        this.currentLevel = 1;
        this.game.states.switchState("play");
	}

    this.level2Button.visible = this.text2.visible = (this.maxLevel >=2);
	if(this.level2Button.isDown){
        level = level2;
        this.currentLevel = 2;
        this.game.states.switchState("play");
	}

    this.level3Button.visible = this.text3.visible = (this.maxLevel >=3);
	if(this.level3Button.isDown){
        level = level3;
        this.currentLevel = 3;
        this.game.states.switchState("play");
	}

    this.level4Button.visible = this.text4.visible = (this.maxLevel >=4);
	if(this.level4Button.isDown){
        level = level4;
        this.currentLevel =4;
        this.game.states.switchState("play");
	}

    this.level5Button.visible = this.text5.visible = (this.maxLevel >=5);
	if(this.level5Button.isDown){
        level = level5;
        this.currentLevel = 5;
        this.game.states.switchState("play");
	}

    this.level6Button.visible = this.text6.visible = (this.maxLevel >=6);
	if(this.level6Button.isDown){
        level = level6;
        this.currentLevel = 6;
		this.game.states.switchState("play");
	}
}
arcadeState.increaseLevel = function() {
    if (this.maxLevel <= this.currentLevel) {
        this.maxLevel = this.currentLevel + 1;
        console.log("New maxLevel " + this.maxLevel);
        this.game.saveManager.localStorage.edit('maxLevel', this.maxLevel, true);
    }
}