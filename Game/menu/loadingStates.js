var loadingState = new Kiwi.State('LoadingState');

loadingState.preload = function(){

	Kiwi.State.prototype.preload.call(this);

	//BACKGROUNDS
		this.addImage('background', 'assets/menu/background.png');

	this.addImage('backgroundPortada', 'assets/menu/Background1.jpg');//Portada.png');
	this.addImage('backgroundInicial', 'assets/menu/Background1.jpg');//Inicial.png');
	this.addImage('backgroundCredits', 'assets/menu/Background1.jpg');//Credits.png');
	this.addImage('backgroundArcade', 'assets/menu/Background1.jpg');//Arcade.png');
	this.addImage('backgroundInfinite', 'assets/menu/Background1.jpg');//Infinite.png');

	//BOTONS PORTADA
	this.addSpriteSheet('leftButton', 'assets/menu/leftButton.png', 51,73);
	this.addSpriteSheet('rightButton', 'assets/menu/rightButton.png', 51,73);
	this.addSpriteSheet('upButton', 'assets/menu/upButton.png', 73, 51);
	this.addSpriteSheet('downButton', 'assets/menu/downButton.png', 73, 51);
    
    //TANQUE
    this.addSpriteSheet('tank_body', 'assets/tank/body.png', 148, 68);
    this.addImage('tank_head',   'assets/tank/head.png');
    this.addImage('tank_cannon', 'assets/tank/cannon.png');
    this.addSpriteSheet('particles', 'assets/particles.png', 170, 170);

    //MAPA
	this.addImage('mapa', 'assets/menu/map.png');

	//BANDERES
	this.addSpriteSheet('desert', 'assets/BANDERES/DESERT-FLAG.png', 89, 51);
	this.addSpriteSheet('island', 'assets/BANDERES/ILAND-FLAG.png', 89, 51);
	this.addSpriteSheet('snow', 'assets/BANDERES/SNOW-FLAG.png', 89, 51);
	this.addSpriteSheet('water', 'assets/BANDERES/WATER-FLAG.png', 89, 51);
	this.addSpriteSheet('wide', 'assets/BANDERES/WIDE-FLAG.png', 89, 51);
	this.addSpriteSheet('wood', 'assets/BANDERES/WOOD-FLAG.png', 89, 51);
	this.addSpriteSheet('beach', 'assets/BANDERES/BEACH-FLAG.png', 89, 51);

    //AUDIO
    this.mouse = this.game.input.mouse;

    this.addSpriteSheet('audioOnOff', 'assets/menu/audioOnOff.png', 60, 60);
    this.addAudio('backgroundMusic', 'backgroundmusic.mp3');

}

loadingState.create = function(){
	Kiwi.State.prototype.create.call(this);
	this.game.states.switchState("PortadaState");
}

loadingState.update = function(){
    Kiwi.State.prototype.update.call(this);
}