var portadaState = new Kiwi.State('PortadaState');

portadaState.create = function(){
	Kiwi.State.prototype.create.call(this);

	//BACKGROUND
    this.background = new Kiwi.GameObjects.StaticImage(this, this.textures['backgroundPortada'], 0, 0);
    this.addChild(this.background);

    //NOM JOC
    this.text1 = new Kiwi.GameObjects.Textfield(this, 'WeaponOs Delux', 170, 200, '#ffffff',  60, 'bold', 'serif');
    this.addChild(this.text1);

    //TEXT
    this.text2 = new Kiwi.GameObjects.Textfield(this, 'Pres ENTER to START', 200, 400, '#ff0000',  40, 'bold', 'serif');
    this.addChild(this.text2);

    //TECLAS
    this.stateKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.ENTER);

    //AUDIO
    this.audioToggle = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['audioOnOff'], 30,20);
    this.audioToggle.animation.add('up', [0],0.1,true,true);
    this.audioToggle.animation.add('down', [0], 0.1, true, true );

    this.backgroundMusic = new Kiwi.Sound.Audio(this.game, 'backgroundMusic', 0.3, true);

    this.backgroundMusic.play();
    this.audioToggle.animation.add('state', [0, 1], 0.1, false);
    this.addChild(this.audioToggle);

}

portadaState.newPress = true;

portadaState.update = function(){
	Kiwi.State.prototype.update.call(this);
    

	if(this.stateKey.isDown){
		this.game.states.switchState("InicialState");
	}

    if(this.audioToggle.isDown) {
        if (newPress) {
            newPress = false;
            if (this.backgroundMusic.isPlaying) {
                this.backgroundMusic.stop();
                this.audioToggle.animation.add('up', [1],0.1,true,true);
                this.audioToggle.animation.add('down', [1], 0.1, true, true );

            } else {
                this.backgroundMusic.play();
                this.audioToggle.animation.add('up', [0],0.1,true,true);
                this.audioToggle.animation.add('down', [0], 0.1, true, true );
            }
        }
    } else {
        newPress = true;
    }
}

