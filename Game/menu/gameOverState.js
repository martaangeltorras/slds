var gameOverState = new Kiwi.State('GameOverState');

gameOverState.create = function(){
    Kiwi.State.prototype.create.call(this);
    this.background = new Kiwi.GameObjects.StaticImage(this, this.textures['backgroundArcade'], 0, 0);

    this.stateKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.ENTER);

    this.addChild(this.background);

    //TEXT
    this.text2 = new Kiwi.GameObjects.Textfield(this, 'GAME OVER', 250, 200, '#000000',  48, 'bold', 'serif');
    this.addChild(this.text2);
    this.text2 = new Kiwi.GameObjects.Textfield(this, 'Pres ENTER to START', 200, 400, '#ff0000',  40, 'bold', 'serif');
    this.addChild(this.text2);
}

gameOverState.update = function(){
    Kiwi.State.prototype.update.call(this);
    
    if (this.stateKey.isDown) {

        this.game.states.switchState("ArcadeState");
    }
}