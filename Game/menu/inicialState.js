var inicialState = new Kiwi.State('InicialState');

inicialState.create = function(){
	Kiwi.State.prototype.create.call(this);

	//BACKGROUND
    this.background = new Kiwi.GameObjects.StaticImage(this, this.textures['backgroundPortada'], 0, 0);
    this.addChild(this.background);

    //NOM JOC
    this.text1 = new Kiwi.GameObjects.Textfield(this, 'WeaponOs Delux', 60, 190, '#ffffff',  48, 'bold', 'serif');
    this.addChild(this.text1);

    //BUTONS
    //ARCADE
    this.arcadeButton = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['rightButton'], 470,150);
	this.addChild(this.arcadeButton);
	this.text2 = new Kiwi.GameObjects.Textfield(this, 'Arcade Mode', 520, 170, '#ffffff',  32, 'bold', 'serif');
    this.addChild(this.text2);

	//INFINITE
	this.infiniteButton = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['rightButton'], 470,250);
	this.addChild(this.infiniteButton);
	this.text3 = new Kiwi.GameObjects.Textfield(this, 'Infinite Mode', 520, 270, '#ffffff',  32, 'bold', 'serif');
    this.addChild(this.text3);

	//CREDITS
	this.creditsButton = new Kiwi.Plugins.GameObjects.TouchButton(this, this.textures['rightButton'], 470,350);
	this.addChild(this.creditsButton);
	this.text4 = new Kiwi.GameObjects.Textfield(this, 'Crèdits', 520, 370, '#ffffff',  32, 'bold', 'serif');
    this.addChild(this.text4);

    //IMATGE
    this.tank = new Kiwi.GameObjects.Sprite(this, this.textures['tank_body'], 180,328);
    this.head = new Kiwi.GameObjects.Sprite(this, this.textures['tank_head'], 215,280);
    this.cannon = new Kiwi.GameObjects.Sprite(this, this.textures['tank_cannon'], 260,300);
    this.cannon.anchorPointX = 0;
    this.cannon.anchorPointY = 10;
    this.tankDirt = new Kiwi.GameObjects.StatelessParticles(this, this.textures['particles'], 180, -110, tankDirtConfig );
    this.tankDirt.startEmitting( true, false );

    this.addChild(this.cannon);
    this.addChild(this.head);
    this.addChild(this.tank);
    this.addChild(this.tankDirt);
}

inicialState.update = function(){

	Kiwi.State.prototype.update.call(this);

	if(this.arcadeButton.isDown){
		this.game.states.switchState("ArcadeState");
	}	

	if(this.infiniteButton.isDown){
        level = null;
		this.game.states.switchState("play");
	}

	if(this.creditsButton.isDown){
		this.game.states.switchState("CreditsState");
	}



    var x = this.game.input.mouse.x - 180
    var y = -this.game.input.mouse.y + 330;
    var a = Math.atan2(x,y);

    this.cannon.rotation = a - Math.PI/2;
    if (this.cannon.rotation > 0.25) this.cannon.rotation = 0.25;
    if (this.cannon.rotation < -0.5) this.cannon.rotation = -0.5;

}