var game = new Kiwi.Game( null, "game", null, { plugins: [ "ParticlesGL","SaveGame" ], renderer: Kiwi.RENDERER_WEBGL }  );
console.log(Kiwi.Plugins);
game.states.addState(loadingState);
game.states.addState(portadaState);
game.states.addState(inicialState);
game.states.addState(arcadeState);
game.states.addState(creditsState);
game.states.addState(gameOverState);
game.states.addState(play);

game.states.switchState("LoadingState");